// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "zmiyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ZMIY_API AzmiyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
