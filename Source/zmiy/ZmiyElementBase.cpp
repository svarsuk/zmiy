// Fill out your copyright notice in the Description page of Project Settings.


#include "ZmiyElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "ZmiyBase.h"

// Sets default values
AZmiyElementBase::AZmiyElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

// Called when the game starts or when spawned
void AZmiyElementBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AZmiyElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
void AZmiyElementBase::SetFirstElementType_Implementation() 
{

	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AZmiyElementBase::HandleBeginOverlap);

}
void AZmiyElementBase::Interact(AActor* Interactor, bool bIsHead)
{
	auto Zmiy = Cast<AZmiyBase>(Interactor);
	if (IsValid(Zmiy)) 
	{
		Zmiy->Destroy();

	}

}

void AZmiyElementBase::HandleBeginOverlap(UPrimitiveComponent*OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{

	if (IsValid(ZmiyOwner)) 
	{

		ZmiyOwner->ZmiyElementOverlap(this,OtherActor);
	}

}

void AZmiyElementBase::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision) 
	{

		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	}

	else 
	{

		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}







