// Fill out your copyright notice in the Description page of Project Settings.


#include "ZmiyBase.h"
#include "ZmiyElementBase.h"
#include "Interactable.h"

// Sets default values
AZmiyBase::AZmiyBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void AZmiyBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddZmiyElement(5);
}

// Called every frame
void AZmiyBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void AZmiyBase::AddZmiyElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i) 
	{

		FVector NewLocation(ZmiyElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		AZmiyElementBase* NewZmiyElem = GetWorld()->SpawnActor<AZmiyElementBase>(ZmiyElementClass, NewTransform);
		NewZmiyElem->ZmiyOwner = this;
		int32 ElemIndex = ZmiyElements.Add(NewZmiyElem);
		if (ElemIndex == 0) 
		{
			NewZmiyElem->SetFirstElementType();

		}
	}
	
}

void AZmiyBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	MovementSpeed = ElementSize;
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeed;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpeed;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovementSpeed;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MovementSpeed;
		break;


	}
	
	ZmiyElements[0]->ToggleCollision();

	for (int i = ZmiyElements.Num() - 1; i > 0; i--)
	{

		auto CurrentElement = ZmiyElements[i];
		auto PrevElement = ZmiyElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);

	}


	ZmiyElements[0]->AddActorWorldOffset(MovementVector); //AddActorWorldOffset(MovementVector);
	ZmiyElements[0]->ToggleCollision();
}

void AZmiyBase::ZmiyElementOverlap(AZmiyElementBase* OverlappedElement,AActor*Other)
{
	if (IsValid(OverlappedElement)) 
	{
		int32 ElemIndex;
		ZmiyElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface) 
		{

			InteractableInterface->Interact(this, bIsFirst);

		}
	}

}

