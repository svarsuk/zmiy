// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ZmiyBase.generated.h"
class AZmiyElementBase;
UENUM()
enum class EMovementDirection 
{
	UP,
	DOWN,
	LEFT,
	RIGHT

};

UCLASS()
class ZMIY_API AZmiyBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AZmiyBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AZmiyElementBase> ZmiyElementClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY()
	TArray<AZmiyElementBase*> ZmiyElements;

	UPROPERTY()
	EMovementDirection LastMoveDirection;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
	void AddZmiyElement(int ElementsNum = 1);
	UFUNCTION(BlueprintCallable)
	void Move();

	UFUNCTION()
	void ZmiyElementOverlap(AZmiyElementBase* OverlappedElement, AActor*Other);
};
