// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef ZMIY_ZmiyElementBase_generated_h
#error "ZmiyElementBase.generated.h already included, missing '#pragma once' in ZmiyElementBase.h"
#endif
#define ZMIY_ZmiyElementBase_generated_h

#define zmiy_Source_zmiy_ZmiyElementBase_h_18_SPARSE_DATA
#define zmiy_Source_zmiy_ZmiyElementBase_h_18_RPC_WRAPPERS \
	virtual void SetFirstElementType_Implementation(); \
 \
	DECLARE_FUNCTION(execToggleCollision); \
	DECLARE_FUNCTION(execHandleBeginOverlap); \
	DECLARE_FUNCTION(execSetFirstElementType);


#define zmiy_Source_zmiy_ZmiyElementBase_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execToggleCollision); \
	DECLARE_FUNCTION(execHandleBeginOverlap); \
	DECLARE_FUNCTION(execSetFirstElementType);


#define zmiy_Source_zmiy_ZmiyElementBase_h_18_EVENT_PARMS
#define zmiy_Source_zmiy_ZmiyElementBase_h_18_CALLBACK_WRAPPERS
#define zmiy_Source_zmiy_ZmiyElementBase_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAZmiyElementBase(); \
	friend struct Z_Construct_UClass_AZmiyElementBase_Statics; \
public: \
	DECLARE_CLASS(AZmiyElementBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/zmiy"), NO_API) \
	DECLARE_SERIALIZER(AZmiyElementBase) \
	virtual UObject* _getUObject() const override { return const_cast<AZmiyElementBase*>(this); }


#define zmiy_Source_zmiy_ZmiyElementBase_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAZmiyElementBase(); \
	friend struct Z_Construct_UClass_AZmiyElementBase_Statics; \
public: \
	DECLARE_CLASS(AZmiyElementBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/zmiy"), NO_API) \
	DECLARE_SERIALIZER(AZmiyElementBase) \
	virtual UObject* _getUObject() const override { return const_cast<AZmiyElementBase*>(this); }


#define zmiy_Source_zmiy_ZmiyElementBase_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AZmiyElementBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AZmiyElementBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AZmiyElementBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AZmiyElementBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AZmiyElementBase(AZmiyElementBase&&); \
	NO_API AZmiyElementBase(const AZmiyElementBase&); \
public:


#define zmiy_Source_zmiy_ZmiyElementBase_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AZmiyElementBase(AZmiyElementBase&&); \
	NO_API AZmiyElementBase(const AZmiyElementBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AZmiyElementBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AZmiyElementBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AZmiyElementBase)


#define zmiy_Source_zmiy_ZmiyElementBase_h_18_PRIVATE_PROPERTY_OFFSET
#define zmiy_Source_zmiy_ZmiyElementBase_h_13_PROLOG \
	zmiy_Source_zmiy_ZmiyElementBase_h_18_EVENT_PARMS


#define zmiy_Source_zmiy_ZmiyElementBase_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	zmiy_Source_zmiy_ZmiyElementBase_h_18_PRIVATE_PROPERTY_OFFSET \
	zmiy_Source_zmiy_ZmiyElementBase_h_18_SPARSE_DATA \
	zmiy_Source_zmiy_ZmiyElementBase_h_18_RPC_WRAPPERS \
	zmiy_Source_zmiy_ZmiyElementBase_h_18_CALLBACK_WRAPPERS \
	zmiy_Source_zmiy_ZmiyElementBase_h_18_INCLASS \
	zmiy_Source_zmiy_ZmiyElementBase_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define zmiy_Source_zmiy_ZmiyElementBase_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	zmiy_Source_zmiy_ZmiyElementBase_h_18_PRIVATE_PROPERTY_OFFSET \
	zmiy_Source_zmiy_ZmiyElementBase_h_18_SPARSE_DATA \
	zmiy_Source_zmiy_ZmiyElementBase_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	zmiy_Source_zmiy_ZmiyElementBase_h_18_CALLBACK_WRAPPERS \
	zmiy_Source_zmiy_ZmiyElementBase_h_18_INCLASS_NO_PURE_DECLS \
	zmiy_Source_zmiy_ZmiyElementBase_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ZMIY_API UClass* StaticClass<class AZmiyElementBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID zmiy_Source_zmiy_ZmiyElementBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
