// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ZMIY_zmiyGameModeBase_generated_h
#error "zmiyGameModeBase.generated.h already included, missing '#pragma once' in zmiyGameModeBase.h"
#endif
#define ZMIY_zmiyGameModeBase_generated_h

#define zmiy_Source_zmiy_zmiyGameModeBase_h_15_SPARSE_DATA
#define zmiy_Source_zmiy_zmiyGameModeBase_h_15_RPC_WRAPPERS
#define zmiy_Source_zmiy_zmiyGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define zmiy_Source_zmiy_zmiyGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAzmiyGameModeBase(); \
	friend struct Z_Construct_UClass_AzmiyGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AzmiyGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/zmiy"), NO_API) \
	DECLARE_SERIALIZER(AzmiyGameModeBase)


#define zmiy_Source_zmiy_zmiyGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAzmiyGameModeBase(); \
	friend struct Z_Construct_UClass_AzmiyGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AzmiyGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/zmiy"), NO_API) \
	DECLARE_SERIALIZER(AzmiyGameModeBase)


#define zmiy_Source_zmiy_zmiyGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AzmiyGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AzmiyGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AzmiyGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AzmiyGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AzmiyGameModeBase(AzmiyGameModeBase&&); \
	NO_API AzmiyGameModeBase(const AzmiyGameModeBase&); \
public:


#define zmiy_Source_zmiy_zmiyGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AzmiyGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AzmiyGameModeBase(AzmiyGameModeBase&&); \
	NO_API AzmiyGameModeBase(const AzmiyGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AzmiyGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AzmiyGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AzmiyGameModeBase)


#define zmiy_Source_zmiy_zmiyGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define zmiy_Source_zmiy_zmiyGameModeBase_h_12_PROLOG
#define zmiy_Source_zmiy_zmiyGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	zmiy_Source_zmiy_zmiyGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	zmiy_Source_zmiy_zmiyGameModeBase_h_15_SPARSE_DATA \
	zmiy_Source_zmiy_zmiyGameModeBase_h_15_RPC_WRAPPERS \
	zmiy_Source_zmiy_zmiyGameModeBase_h_15_INCLASS \
	zmiy_Source_zmiy_zmiyGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define zmiy_Source_zmiy_zmiyGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	zmiy_Source_zmiy_zmiyGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	zmiy_Source_zmiy_zmiyGameModeBase_h_15_SPARSE_DATA \
	zmiy_Source_zmiy_zmiyGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	zmiy_Source_zmiy_zmiyGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	zmiy_Source_zmiy_zmiyGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ZMIY_API UClass* StaticClass<class AzmiyGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID zmiy_Source_zmiy_zmiyGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
