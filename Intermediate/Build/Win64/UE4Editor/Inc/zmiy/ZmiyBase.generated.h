// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AZmiyElementBase;
class AActor;
#ifdef ZMIY_ZmiyBase_generated_h
#error "ZmiyBase.generated.h already included, missing '#pragma once' in ZmiyBase.h"
#endif
#define ZMIY_ZmiyBase_generated_h

#define zmiy_Source_zmiy_ZmiyBase_h_22_SPARSE_DATA
#define zmiy_Source_zmiy_ZmiyBase_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execZmiyElementOverlap); \
	DECLARE_FUNCTION(execMove); \
	DECLARE_FUNCTION(execAddZmiyElement);


#define zmiy_Source_zmiy_ZmiyBase_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execZmiyElementOverlap); \
	DECLARE_FUNCTION(execMove); \
	DECLARE_FUNCTION(execAddZmiyElement);


#define zmiy_Source_zmiy_ZmiyBase_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAZmiyBase(); \
	friend struct Z_Construct_UClass_AZmiyBase_Statics; \
public: \
	DECLARE_CLASS(AZmiyBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/zmiy"), NO_API) \
	DECLARE_SERIALIZER(AZmiyBase)


#define zmiy_Source_zmiy_ZmiyBase_h_22_INCLASS \
private: \
	static void StaticRegisterNativesAZmiyBase(); \
	friend struct Z_Construct_UClass_AZmiyBase_Statics; \
public: \
	DECLARE_CLASS(AZmiyBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/zmiy"), NO_API) \
	DECLARE_SERIALIZER(AZmiyBase)


#define zmiy_Source_zmiy_ZmiyBase_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AZmiyBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AZmiyBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AZmiyBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AZmiyBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AZmiyBase(AZmiyBase&&); \
	NO_API AZmiyBase(const AZmiyBase&); \
public:


#define zmiy_Source_zmiy_ZmiyBase_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AZmiyBase(AZmiyBase&&); \
	NO_API AZmiyBase(const AZmiyBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AZmiyBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AZmiyBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AZmiyBase)


#define zmiy_Source_zmiy_ZmiyBase_h_22_PRIVATE_PROPERTY_OFFSET
#define zmiy_Source_zmiy_ZmiyBase_h_19_PROLOG
#define zmiy_Source_zmiy_ZmiyBase_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	zmiy_Source_zmiy_ZmiyBase_h_22_PRIVATE_PROPERTY_OFFSET \
	zmiy_Source_zmiy_ZmiyBase_h_22_SPARSE_DATA \
	zmiy_Source_zmiy_ZmiyBase_h_22_RPC_WRAPPERS \
	zmiy_Source_zmiy_ZmiyBase_h_22_INCLASS \
	zmiy_Source_zmiy_ZmiyBase_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define zmiy_Source_zmiy_ZmiyBase_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	zmiy_Source_zmiy_ZmiyBase_h_22_PRIVATE_PROPERTY_OFFSET \
	zmiy_Source_zmiy_ZmiyBase_h_22_SPARSE_DATA \
	zmiy_Source_zmiy_ZmiyBase_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	zmiy_Source_zmiy_ZmiyBase_h_22_INCLASS_NO_PURE_DECLS \
	zmiy_Source_zmiy_ZmiyBase_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ZMIY_API UClass* StaticClass<class AZmiyBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID zmiy_Source_zmiy_ZmiyBase_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EMovementDirection::UP) \
	op(EMovementDirection::DOWN) \
	op(EMovementDirection::LEFT) \
	op(EMovementDirection::RIGHT) 

enum class EMovementDirection;
template<> ZMIY_API UEnum* StaticEnum<EMovementDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
